;;; -*- no-byte-compile: t -*-
(define-package "tern" "20181108.722" "Tern-powered JavaScript integration" '((json "1.2") (cl-lib "0.5") (emacs "24")) :commit "8436f470d538798f94612902a3ff8178b408933a" :authors '(("Marijn Haverbeke")) :maintainer '("Marijn Haverbeke") :url "http://ternjs.net/")

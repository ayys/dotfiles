;;; smart-dash-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "smart-dash" "smart-dash.el" (0 0 0 0))
;;; Generated autoloads from smart-dash.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "smart-dash" '("smart-dash-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; smart-dash-autoloads.el ends here

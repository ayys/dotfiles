;ELC   
;;; Compiled
;;; in Emacs version 26.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301\302\303\304\305%\210\306\307\310\311\312\301\313\314&\207" [custom-declare-group smart-dash nil "Intelligently insert either a dash or an underscore depending\non context." :prefix "smart-dash" custom-declare-variable smart-dash-c-modes '(c-mode c++-mode objc-mode) "Major modes in which _> should be replaced by the -> struct\npointer member access operator and __ should be replaced by the\n-- post-decrement operator." :group :type (repeat symbol)] 8)
#@32 Key map for `smart-dash-mode'.
(defconst smart-dash-mode-keymap (byte-code "\301\302\303\304BD\305\306\300!\205 \305$\207" [smart-dash-mode-keymap easy-mmode-define-keymap ("-" . smart-dash-insert) [kp-subtract] smart-dash-insert-dash nil boundp] 5) (#$ . 885))
#@99 Non-nil if Smart-Dash mode is enabled.
Use the command `smart-dash-mode' to change this variable.
(defvar smart-dash-mode nil (#$ . 1155))
(make-variable-buffer-local 'smart-dash-mode)
#@1029 Redefine the dash key to insert an underscore within C-style
identifiers and a dash otherwise.  This allows you to type
all_lowercase_c_identifiers as comfortably as you would
lisp-style-identifiers.

While Smart-Dash mode is active, you can type \[quoted-insert] -
or use the minus key on the numeric keypad to override it and
insert a dash after a C-style identifier character.  You might
need to do this if you want to type a cramped-looking expression
like x-5.

If Smart-Dash mode is activated while in a C-like mode (c-mode,
c++-mode, and objc-mode by default, customizable with
`smart-dash-c-modes') it will also activate Smart-Dash-C mode,
which translates "_>" into "->" and "__" into "--"
automatically so that struct pointer member access and
postfix-decrement aren't made more difficult by Smart-Dash mode's
tendency to insert underscores at the tail ends of identifiers
whether you want it to or not.  Note that this will necessitate
that you type literal underscores if you want more than one
underscore in a row.
(defalias 'smart-dash-mode #[(&optional arg) "\306 	\307=\203 \n?\202 \310	!\311V\211\203( \f>\203\" \312\313!\210\314 \210\202/ \315 \210\312\311!\210\316\317\n\2039 \320\202: \321\"\210\322\323!\203_ \306 \203N \306 \232\203_ \324\325\326\n\203Z \327\202[ \330#\210))\331 \210\n\207" [#1=#:last-message arg smart-dash-mode major-mode smart-dash-c-modes local current-message toggle prefix-numeric-value 0 smart-dash-c-mode 1 smart-dash-isearch-install smart-dash-isearch-uninstall run-hooks smart-dash-mode-hook smart-dash-mode-on-hook smart-dash-mode-off-hook called-interactively-p any " in current buffer" message "Smart-Dash mode %sabled%s" "en" "dis" force-mode-line-update] 5 (#$ . 1348) (list (or current-prefix-arg 'toggle))])
(defvar smart-dash-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\211%\207" [smart-dash-mode-keymap smart-dash-mode-hook variable-documentation put "Hook run after entering or leaving `smart-dash-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode smart-dash-mode "" nil] 6)
#@97 Key map supplement for `smart-dash-mode' when in a C-like
major mode.  See `smart-dash-c-modes'
(defconst smart-dash-c-mode-keymap (byte-code "\301\302\303\304\300!\205\n \303$\207" [smart-dash-c-mode-keymap easy-mmode-define-keymap ((">" . smart-dash-insert-gt)) nil boundp] 5) (#$ . 3535))
#@103 Non-nil if Smart-Dash-C mode is enabled.
Use the command `smart-dash-c-mode' to change this variable.
(defvar smart-dash-c-mode nil (#$ . 3835))
(make-variable-buffer-local 'smart-dash-c-mode)
#@355 Set the > key to call `smart-dash-insert-gt'.  Also modifies
the behavior of the dash key so that the postfix-- operator can
be typed normally (but shift will be needed for typing more than
one underscore in a row).

DO NOT ACTIVATE THIS MINOR MODE DIRECTLY.  Smart-Dash mode will
activate it if the current major mode is listed in
`smart-dash-c-modes'.
(defalias 'smart-dash-c-mode #[(&optional arg) "\304 	\305=\203 \n?\202 \306	!\307V\211\203 \310 \210\202! \311 \210\312\313\n\203+ \314\202, \315\"\210\316\317!\203Q \304 \203@ \304 \232\203Q \320\321\322\n\203L \323\202M \324#\210))\325 \210\n\207" [#1=#:last-message arg smart-dash-c-mode local current-message toggle prefix-numeric-value 0 smart-dash-c-isearch-install smart-dash-c-isearch-uninstall run-hooks smart-dash-c-mode-hook smart-dash-c-mode-on-hook smart-dash-c-mode-off-hook called-interactively-p any " in current buffer" message "Smart-Dash-C mode %sabled%s" "en" "dis" force-mode-line-update] 5 (#$ . 4035) (list (or current-prefix-arg 'toggle))])
(defvar smart-dash-c-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\211%\207" [smart-dash-c-mode-keymap smart-dash-c-mode-hook variable-documentation put "Hook run after entering or leaving `smart-dash-c-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode smart-dash-c-mode "" nil] 6)
(defalias 'smart-dash-in-regular-code-p #[nil "\300 \3058\3068\205 \307\310\212\311 \210`)\"	?\205% \n?\205% \f?,\207" [syntax-ppss in-string in-comment smart-dash-c-mode in-include 3 4 looking-back " *# *include +[<\"].*" beginning-of-line] 3])
(defalias 'smart-dash-char-before-point #[(&optional n) "\204 \301`\\\206 `Sf\207" [n 0] 2])
(defalias 'smart-dash-do-insert #[(insertf deletef bobpf char-before-f regcodepf) "\203 \306\202	 \307\n \203a  \204a \310	\311\f !\"\203$ \312!\202d \203; \313\312\f \"\203; \314!\210\315!\202d \203[ \313\316\f \"\203[ \313\316\f\317!\"\203[ \320!\210\321!\202d \316!\202d \316!)\207" [smart-dash-c-mode ident-re regcodepf bobpf char-before-f insertf "[A-Za-z0-9]" "[A-Za-z0-9_]" string-match string 95 eql 1 "--" 45 -1 2 "_--" deletef] 4])
#@127 Insert an underscore following [A-Za-z0-9_], a dash otherwise.

If `smart-dash-c-mode' is activated, also replace __ with --.
(defalias 'smart-dash-insert #[nil "\300\301\302\303\304\305%\207" [smart-dash-do-insert insert delete-backward-char bobp smart-dash-char-before-point smart-dash-in-regular-code-p] 6 (#$ . 6297) nil])
#@55 Insert a dash regardless of the preceeding character.
(defalias 'smart-dash-insert-dash #[nil "\300c\207" [45] 1 (#$ . 6630) nil])
(defalias 'smart-dash-do-insert-gt #[(insertf deletef bobpf char-before-f codepf) " \204 	 \203 \n \305U\203 \306!\210\f\307!\207\f\310!\207" [bobpf codepf char-before-f deletef insertf 95 1 "->" 62] 2])
#@189 Insert a greater-than symbol.  If the preceeding character is
an underscore, replace it with a dash.

This behavior is desirable in order to make struct pointer member
access comfortable.
(defalias 'smart-dash-insert-gt #[nil "\300\301\302\303\304\305%\207" [smart-dash-do-insert-gt insert delete-backward-char bobp smart-dash-char-before-point smart-dash-in-regular-code-p] 6 (#$ . 6977) nil])
(defalias 'smart-dash-isearch-in-regular-code-p #[nil "\300\207" [t] 1])
(defalias 'smart-dash-isearch-string #[(s) ";\204	 \301!\302\303\"\207" [s string mapc #[(c) "\302 )\207" [c last-command-event isearch-printing-char] 1]] 3])
(defalias 'smart-dash-isearch-del-char #[(&optional n) "\204 \301\302V\205 \303 \210S\211\202 \207" [n 1 0 isearch-del-char] 3])
(defalias 'smart-dash-isearch-char-before #[(&optional n) "\204 \302\3031 	S\302U?\205 O\302\2340\207\210\304\207" [n isearch-string 0 (args-out-of-range) nil] 4])
(defalias 'smart-dash-isearch-bobp #[nil "G\301U\207" [isearch-string 0] 2])
#@145 Isearch for an underscore following [A-Za-z0-9_], a dash otherwise.

If `smart-dash-c-mode' is activated, also replace __ with -- in
isearches.
(defalias 'smart-dash-isearch-insert #[nil "\300\301\302\303\304\305%\207" [smart-dash-do-insert smart-dash-isearch-string smart-dash-isearch-del-char smart-dash-isearch-bobp smart-dash-isearch-char-before smart-dash-isearch-in-regular-code-p] 6 (#$ . 8004) nil])
#@60 Isearch for a dash regardless of the preceeding character.
(defalias 'smart-dash-isearch-insert-dash #[nil "\300\301!\207" [smart-dash-isearch-string 45] 2 (#$ . 8418) nil])
(defalias 'smart-dash-isearch-install #[nil "\302\300!\210\303	!)\304\305\306#\210\304\307\310#\207" [isearch-mode-map map make-local-variable copy-keymap define-key "-" smart-dash-isearch-insert [kp-subtract] smart-dash-isearch-insert-dash] 4])
(defalias 'smart-dash-isearch-uninstall #[nil "\301\300!\205 \302\303\304#\210\302\305\304#\207" [isearch-mode-map local-variable-p define-key "-" isearch-printing-char [kp-subtract]] 4])
#@208 Isearch for a greater-than symbol.  If the preceeding
character is an underscore, replace it with a dash.

This behavior is desirable in order to make searching for struct
pointer member access comfortable.
(defalias 'smart-dash-isearch-insert-gt #[nil "\300\301\302\303\304\305%\207" [smart-dash-do-insert-gt smart-dash-isearch-string smart-dash-isearch-del-char smart-dash-isearch-bobp smart-dash-isearch-char-before smart-dash-isearch-in-regular-code-p] 6 (#$ . 9041) nil])
(defalias 'smart-dash-c-isearch-install #[nil "\302\300!\210\303	!)\304\305\306#\207" [isearch-mode-map map make-local-variable copy-keymap define-key ">" smart-dash-isearch-insert-gt] 4])
(defalias 'smart-dash-c-isearch-uninstall #[nil "\301\300!\205 \302\303\304#\207" [isearch-mode-map local-variable-p define-key ">" isearch-printing-char] 4])
(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\306\301\314\315&\210\310\316\302\317\306\301\314\315&\210\310\320\321\322\306\301\314\323&\210\310\324\325\326\306\301\314\327&\207" [custom-declare-group smart-dash-minibuffer nil "Activate Smart-Dash mode in the minibuffer\nfor commands that are executed in Smart-Dash mode buffers." :prefix "smart-dash-minibuffer" :group smart-dash custom-declare-variable smart-dash-minibuffer-enabled t "If non-nil, activate Smart-Dash mode in the minibuffer for\nsome commands.\n\nThe decision of which commands will have Smart-Dash mode\nactivated in is controlled by `smart-dash-minibuffer-by-default',\n`smart-dash-minibuffer-allow-commands' and\n`smart-dash-minibuffer-deny-commands'." :type boolean smart-dash-minibuffer-by-default "If non-nil, activate Smart-Dash mode in the minibuffer for all\ncommands except those listed in `smart-dash-minibuffer-deny-commands'.\n\nIf nil (the default), activate Smart-Dash mode in the minibuffer\nonly for commands listed in `smart-dash-minibuffer-allow-commands'." smart-dash-minibuffer-allow-commands '(query-replace query-replace-regexp replace-string replace-regexp search-forward search-forward-regexp isearch-edit-string string-rectangle find-tag grep) "Commands whose minibuffer inputs should have Smart-Dash mode\nactivated when `smart-dash-minibuffer-by-default' is nil." (repeat function) smart-dash-minibuffer-deny-commands '(execute-extended-command eval-expression find-file) "Commands whose minibuffer inputs should NOT have Smart-Dash\nmode activated when `smart-dash-minibuffer-by-default' is\nnon-nil." (repeat function)] 8)
(defvar smart-dash-minibuffer-last-exit-command-stack (list nil))
(defvar smart-dash-minibuffer-this-command-stack (list nil))
(defalias 'smart-dash-minibuffer-exit #[nil "A	A\n\240\207" [smart-dash-minibuffer-last-exit-command-stack smart-dash-minibuffer-this-command-stack this-command] 2])
(add-hook 'minibuffer-exit-hook 'smart-dash-minibuffer-exit)
(defalias 'smart-dash-minibuffer-install #[nil "	@=\204 \n\240\210\306\307 !rq\210\f)rq\210)\n\n@\f\205< \2037 >?\202< >\211\203Q \203I \304 \210\n\203Q \310 \210-\311	B\311\nB\211\207" [this-command smart-dash-minibuffer-last-exit-command-stack smart-dash-minibuffer-this-command-stack selected-buffer smart-dash-mode sd-active window-buffer minibuffer-selected-window smart-dash-c-mode nil sd-c-active local-this-command smart-dash-minibuffer-enabled smart-dash-minibuffer-by-default smart-dash-minibuffer-deny-commands smart-dash-minibuffer-allow-commands allow] 3])
(byte-code "\300\301\302\"\210\303\304!\207" [add-hook minibuffer-setup-hook smart-dash-minibuffer-install provide smart-dash] 3)

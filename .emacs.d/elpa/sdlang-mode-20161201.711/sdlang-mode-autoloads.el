;;; sdlang-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "sdlang-mode" "sdlang-mode.el" (0 0 0 0))
;;; Generated autoloads from sdlang-mode.el

(autoload 'sdlang-mode "sdlang-mode" "\
Major mode for editing Simple Declarative Language files.

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.sdl\\'" . sdlang-mode))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sdlang-mode" '("sdlang-mode-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sdlang-mode-autoloads.el ends here

;;; -*- no-byte-compile: t -*-
(define-package "sdlang-mode" "20161201.711" "Major mode for Simple Declarative Language files." '((emacs "24.3")) :commit "d42a6eedefeb44919fbacf58d302b6df18f05bbc" :keywords '("languages") :authors '(("Vladimir Panteleev")) :maintainer '("Vladimir Panteleev") :url "https://github.com/CyberShadow/sdlang-mode")

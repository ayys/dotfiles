;;; -*- no-byte-compile: t -*-
(define-package "cmake-mode" "20190710.1319" "major-mode for editing CMake sources" '((emacs "24.1")) :commit "a6d95f57cc8934813ba273e1016d03bb190c44c0")

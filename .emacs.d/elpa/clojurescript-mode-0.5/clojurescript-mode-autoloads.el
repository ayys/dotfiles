;;; clojurescript-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "clojurescript-mode" "clojurescript-mode.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from clojurescript-mode.el

(autoload 'clojurescript-mode "clojurescript-mode" "\
Major mode for ClojureScript

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.cljs$" . clojurescript-mode))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "clojurescript-mode" '("clojurescript-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; clojurescript-mode-autoloads.el ends here

;;; -*- no-byte-compile: t -*-
(define-package "js-auto-format-mode" "20180807.1352" "Minor mode for auto-formatting JavaScript code" '((emacs "24")) :commit "17df4436cffeb426eb9a9e8b9220e74af0a7d18d" :keywords '("languages") :authors '(("Masafumi Koba" . "ybiquitous@gmail.com")) :maintainer '("Masafumi Koba" . "ybiquitous@gmail.com") :url "https://github.com/ybiquitous/js-auto-format-mode")

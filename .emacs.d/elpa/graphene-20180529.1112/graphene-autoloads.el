;;; graphene-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "graphene-editing" "graphene-editing.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from graphene-editing.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "graphene-editing" '("graphene-")))

;;;***

;;;### (autoloads nil "graphene-helper-functions" "graphene-helper-functions.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from graphene-helper-functions.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "graphene-helper-functions" '("insert-semicolon-at-end-of-line" "increase-window-" "decrease-window-" "newline-anywhere" "create-new-buffer" "comment-current-line-dwim" "kill-")))

;;;***

;;;### (autoloads nil "graphene-look" "graphene-look.el" (0 0 0 0))
;;; Generated autoloads from graphene-look.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "graphene-look" '("graphene-")))

;;;***

;;;### (autoloads nil "graphene-projects" "graphene-projects.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from graphene-projects.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "graphene-projects" '("pid-if-active-emacs-process" "emacs-process-p" "graphene-")))

;;;***

;;;### (autoloads nil nil ("graphene-env.el" "graphene-linux-defaults.el"
;;;;;;  "graphene-osx-defaults.el" "graphene-other-defaults.el" "graphene-pkg.el"
;;;;;;  "graphene-smartparens-config.el" "graphene-windows-defaults.el"
;;;;;;  "graphene.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; graphene-autoloads.el ends here

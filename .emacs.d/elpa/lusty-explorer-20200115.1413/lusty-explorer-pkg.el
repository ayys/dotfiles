;;; -*- no-byte-compile: t -*-
(define-package "lusty-explorer" "20200115.1413" "Dynamic filesystem explorer and buffer switcher" '((emacs "24.3") (s "1.11.0")) :commit "3f4be19e0c466399c680a355c402b09792123e4d" :keywords '("convenience" "files" "matching" "tools") :url "https://github.com/sjbach/lusty-emacs")

;;; yaoddmuse-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "yaoddmuse" "yaoddmuse.el" (0 0 0 0))
;;; Generated autoloads from yaoddmuse.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "yaoddmuse" '("yaoddmuse-" "emacswiki")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; yaoddmuse-autoloads.el ends here

;;; -*- no-byte-compile: t -*-
(define-package "smart-dash" "20200104.1620" "Smart-Dash minor mode" 'nil :commit "cc540eea7452e15d4ef2b09d8809d88174f509c0" :authors '(("Dennis Lambe Jr." . "malsyned@malsyned.net")) :maintainer '("Dennis Lambe Jr." . "malsyned@malsyned.net"))

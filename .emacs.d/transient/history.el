((magit-am
  ("--3way"))
 (magit-branch nil)
 (magit-commit nil)
 (magit-diff
  ("--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-merge nil)
 (magit-pull nil)
 (magit-push nil)
 (magit-rebase nil)
 (magit-reset nil)
 (magit-revert
  ("--edit")))

set -g prefix C-z                        # GNU-Screen compatible prefix
bind C-z send-prefix -1

bind -n S-Left previous-window
bind -n S-Right next-window
bind-key -n S-Up swap-window -t -1
bind-key -n S-Down swap-window -t +1
set -g default-terminal "screen-256color"
set -g history-limit 99999
set -g status-interval 5
set -g status-justify center
set -g status-position top
set -g status-left '#[fg=yellow]🐈#[fg=green][#(whoami)@#(hostname)]#[default] '
set -g status-right '#[fg=yellow] #(date +"%A, %d %B %Y - %R") #[default]'
set -g visual-activity off
set -s escape-time 0
set-option -g base-index 1
set-option -g message-bg brightred
set-option -g message-fg white
set-option -g pane-active-border-bg black
set-option -g pane-active-border-fg green
set-option -g pane-border-bg black
set-option -g pane-border-fg green
set-option -g set-titles on
set-option -g set-titles-string "#T"
set-option -g status on
set-option -g status-attr dim
set-option -g status-bg colour235
set-option -g terminal-overrides 'xterm*:smcup@:rmcup@'
set-option -gw window-status-activity-bg colour235
set-option -gw window-status-activity-fg red
set-option -gw xterm-keys on
set-window-option -g window-status-attr dim
set-window-option -g window-status-bg colour235
set-window-option -g window-status-current-attr bright
set-window-option -g window-status-current-bg colour240
set-window-option -g window-status-current-fg white
set-window-option -g window-status-fg white
set-window-option -g xterm-keys on
setw -g aggressive-resize on
setw -g automatic-rename
setw -g monitor-activity on
setw -g pane-base-index 1
unbind-key -n C-Left
unbind-key -n C-Right

# reload configuration
bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'
set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows

setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed

set -g set-titles on          # set terminal title

set -g display-panes-time 800 # slightly longer pane indicators display time
set -g display-time 1000      # slightly longer status messages display time

set -g status-interval 10     # redraw status line every 10 seconds

# clear both screen and history
bind -n C-l send-keys C-l \; run 'sleep 0.1' \; clear-history

# activity
set -g monitor-activity on
set -g visual-activity off


# -- navigation ----------------------------------------------------------------

# create session
bind C-c new-session

# find session
bind C-f command-prompt -p find-session 'switch-client -t %%'

# split current window horizontally
bind - split-window -v
# split current window vertically
bind _ split-window -h

# pane navigation
bind -r h select-pane -L  # move left
bind -r j select-pane -D  # move down
bind -r k select-pane -U  # move up
bind -r l select-pane -R  # move right
bind > swap-pane -D       # swap current pane with the next one
bind < swap-pane -U       # swap current pane with the previous one

# maximize current pane
bind + run 'cut -c3- ~/.tmux.conf | sh -s _maximize_pane "#{session_name}" #D'

# pane resizing
bind -r H resize-pane -L 2
bind -r J resize-pane -D 2
bind -r K resize-pane -U 2
bind -r L resize-pane -R 2

# window navigation
unbind n
unbind p
bind -r C-h previous-window # select previous window
bind -r C-l next-window     # select next window
bind Tab last-window        # move to last active window

# toggle mouse
bind m run "cut -c3- ~/.tmux.conf | sh -s _toggle_mouse"


# -- urlview -------------------------------------------------------------------

bind U run "cut -c3- ~/.tmux.conf | sh -s _urlview #{pane_id}"


# -- facebook pathpicker -------------------------------------------------------

bind F run "cut -c3- ~/.tmux.conf | sh -s _fpp #{pane_id}"

;;; package --- Summary

;; ;;; commentary:
;; ;; Added by Package.el.  This must come before configurations of
;; ;; installed packages.  Don't delete this line.  If you don't want it,
;; ;; just comment it out by adding a semicolon to the start of the line.
;; ;; You may delete these explanatory comments.
;; ;;; Code:

;; Make startup faster by reducing the frequency of garbage
;; collection.  The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

;; The rest of the init file.

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;; Faster than the default scp
(setq tramp-default-method "ssh")

;; load emacs MELPA
;; load emacs 24's package system. Add MELPA repository.

(setq package-archives '(
 			 ;; ("marmalade" . "https://marmalade-repo.org/packages/")
 			 ("melpa" . "http://melpa.org/packages/")
 			 ("elpa" . "http://elpa.gnu.org/packages/")
 			 ("gnu" . "http://elpa.gnu.org/packages/")))

(set-default 'cursor-type 'bar)

(package-initialize)
(require 'package)
(require 'org)
(require 'dired)
(require 'helm-fuzzier)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-term-color-vector
   [unspecified "#2d2a2e" "#ff6188" "#a9dc76" "#ffd866" "#78dce8" "#ab9df2" "#ff6188" "#fcfcfa"] t)
 '(blink-cursor-mode t)
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(compilation-message-face (quote default))
 '(custom-safe-themes
   (quote
    ("fd944f09d4d0c4d4a3c82bd7b3360f17e3ada8adf29f28199d09308ba01cc092" "5ed25f51c2ed06fc63ada02d3af8ed860d62707e96efc826f4a88fd511f45a1d" "37ba833442e0c5155a46df21446cadbe623440ccb6bbd61382eb869a2b9e9bf9" "09cadcc2784baa744c6a7c5ebf2a30df59c275414768b0719b800cabd8d1b842" "585942bb24cab2d4b2f74977ac3ba6ddbd888e3776b9d2f993c5704aa8bb4739" "8f97d5ec8a774485296e366fdde6ff5589cf9e319a584b845b6f7fa788c9fa9a" "73c69e346ec1cb3d1508c2447f6518a6e582851792a8c0e57a22d6b9948071b4" "9fe9a86557144c43f9008daaf4bbee29498e30a3d957f6cf42b63b35cf598fd1" "6b289bab28a7e511f9c54496be647dc60f5bd8f9917c9495978762b99d8c96a0" "d1b4990bd599f5e2186c3f75769a2c5334063e9e541e37514942c27975700370" "6b3ec7b218feca3b56e00554cd47c3a465ad423e4366f27af5e3ed895129e734" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "1436d643b98844555d56c59c74004eb158dc85fc55d2e7205f8d9b8c860e177f" "4639288d273cbd3dc880992e6032f9c817f17c4a91f00f3872009a099f5b3f84" "100e7c5956d7bb3fd0eebff57fde6de8f3b9fafa056a2519f169f85199cc1c96" "8e797edd9fa9afec181efbfeeebf96aeafbd11b69c4c85fa229bb5b9f7f7e66c" "274fa62b00d732d093fc3f120aca1b31a6bb484492f31081c1814a858e25c72e" "de1f10725856538a8c373b3a314d41b450b8eba21d653c4a4498d52bb801ecd2" "54f2d1fcc9bcadedd50398697618f7c34aceb9966a6cbaa99829eb64c0c1f3ca" "fe666e5ac37c2dfcf80074e88b9252c71a22b6f5d2f566df9a7aa4f9bea55ef8" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36" "bd7b7c5df1174796deefce5debc2d976b264585d51852c962362be83932873d9" "13d20048c12826c7ea636fbe513d6f24c0d43709a761052adbca052708798ce3" "e61752b5a3af12be08e99d076aedadd76052137560b7e684a8be2f8d2958edc3" "a22f40b63f9bc0a69ebc8ba4fbc6b452a4e3f84b80590ba0a92b4ff599e53ad0" "11e57648ab04915568e558b77541d0e94e69d09c9c54c06075938b6abc0189d8" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default)))
 '(dired-listing-switches "-alh")
 '(enable-recursive-minibuffers t)
 '(fci-rule-color "#3C3D37")
 '(global-company-mode t)
 '(highlight-changes-colors (quote ("#FD5FF0" "#AE81FF")))
 '(highlight-tail-colors
   (quote
    (("#3C3D37" . 0)
     ("#679A01" . 20)
     ("#4BBEAE" . 30)
     ("#1DB4D0" . 50)
     ("#9A8F21" . 60)
     ("#A75B00" . 70)
     ("#F309DF" . 85)
     ("#3C3D37" . 100))))
 '(ibuffer-expert t)
 '(ibuffer-modified-char 42)
 '(ibuffer-old-time 24)
 '(idle-highlight-idle-time 0)
 '(ivy-mode t)
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(jdee-server-dir "/code/jdee")
 '(js2-include-node-externs t)
 '(magit-diff-use-overlays nil)
 '(minibuffer-electric-default-mode t)
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(objed-cursor-color "#ff6c6b")
 '(package-selected-packages
   (quote
    (commander f s modus-vivendi-theme modus-operandi-theme sdlang-mode ob-sql-mode rust-mode darkokai-theme monokai-pro-theme lex bison-mode elpy company-jedi jedi pymacs epresent org-present pipenv pyenv-mode anti-zenburn-theme zenburn faff-theme which-key flymake-yaml counsel-tramp helm-tramp helm-fuzzier lusty-explorer counsel swiper swiper-helm redis js-auto-format-mode markdown-preview-mode nginx-mode org-protocol-jekyll docker clojurescript-mode clojure-mode edn yaml-tomato yaml-imenu ac-c-headers flyspell-lazy cask cmake-ide flycheck-dmd-dub overcast-theme mingus select-themes csv-mode csv makefile-runner org-beautify-theme rainbow-delimiters paredit pretty-lambdada brutalist-theme adoc-mode systemd lua-mode dockerfile-mode gitlab-ci-mode org-pdfview ac-html markdownfmt molokai-theme org-bullets org-readme monokai-theme doom-themes typescript-mode angular-mode groovy-imports nyan-mode wget ibuffer-projectile avy web-beautify smart-dash symbols-mode d-mode sudo-edit volatile-highlights graphene ansi package-build epl git dash guile-scheme cmake-mode kotlin-mode groovy-mode geiser zenburn-theme irony markdown-mode+ ac-etags ac-clang ac-js2 tern-auto-complete tern gruvbox-theme smart-mode-line doom telephone-line dired-details projectile moe-theme undo-tree npm-mode gradle-mode arduino-mode java-file-create java-imports javadoc-lookup javarun flymake-json sqlite dracula-theme js2-closure restclient-test restclient multiple-cursors js2-mode nodejs-repl json-mode flycheck rainbow-mode highlight-symbol expand-region magit idle-highlight-mode)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(pos-tip-background-color "#FFFACE")
 '(pos-tip-foreground-color "#272822")
 '(show-paren-mode t)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#F92672")
     (40 . "#CF4F1F")
     (60 . "#C26C0F")
     (80 . "#E6DB74")
     (100 . "#AB8C00")
     (120 . "#A18F00")
     (140 . "#989200")
     (160 . "#8E9500")
     (180 . "#A6E22E")
     (200 . "#729A1E")
     (220 . "#609C3C")
     (240 . "#4E9D5B")
     (260 . "#3C9F79")
     (280 . "#A1EFE4")
     (300 . "#299BA6")
     (320 . "#2896B5")
     (340 . "#2790C3")
     (360 . "#66D9EF"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0")))
 '(which-key-mode t))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(idle-highlight ((f (:background "#fff")))))

;;;;;;;;;;;;;;;;;;;;;;; CALL SOME FUNCTIONS

;;; Code:
(helm-fuzzier-mode 1)
;; (load-theme 'dracula)
(volatile-highlights-mode t)
(smart-mode-line-enable)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(pending-delete-mode)
(ido-mode t)
(projectile-mode +1)
(global-visual-line-mode t)

;; ;; dont ring the bell!!
(setq visible-bell       nil
      ring-bell-function #'ignore)
;; ;;;;;;;;;;;;;;;;;;;;;;; ADD TO LISTS
(add-hook 'prog-mode-hook 'rainbow-mode)
(add-hook 'eshell-mode-hook 'shell-switcher-manually-register-shell)
(add-hook  'prog-mode-hook #'electric-pair-mode)
(add-hook  'prog-mode-hook #'highlight-symbol-mode)
(add-hook 'c++-mode-hook #'irony-mode)
(add-hook 'c-mode-hook #'irony-mode)
(add-hook 'ibuffer-hook
    (lambda ()
      (ibuffer-projectile-set-filter-groups)))

(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq ibuffer-formats
      '((mark modified read-only " "
              (name 18 18 :left :elide)
              " "
              (size 9 -1 :right)
              " "
           (mode 16 16 :left :elide)
              " "
              project-relative-file)))


(add-to-list 'auto-mode-alist '("build.gradle" . groovy-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.ts\\'" . angular-mode))
(add-to-list 'auto-mode-alist '("\\.rest\\'" . restclient-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(put 'dired-find-alternate-file 'disabled nil)
;;;;;;;;;;;;;;;; ADD HOOKS
(add-hook 'prog-mode-hook (lambda () (company-mode t)))
(add-hook 'prog-mode-hook (lambda () (auto-complete-mode t)))
(add-hook 'after-init-hook #'global-flycheck-mode)

(autoload 'pymacs-apply "pymacs")
(autoload 'pymacs-call "pymacs")
(autoload 'pymacs-eval "pymacs" nil t)
(autoload 'pymacs-exec "pymacs" nil t)
(autoload 'pymacs-load "pymacs" nil t)

;;;;;;;;;;;;;;;; DIRED DETAILS
(setq projectile-project-search-path '("/code/git"))
(setq-default dired-details-hidden-string "--- ")
(setq org-catch-invisible-edits 'show-and-error)

(add-hook 'ibuffer-mode-hook
	  '(lambda ()
             (ibuffer-auto-mode 1)
             (visual-line-mode 0)
             (toggle-truncate-lines 1)
	     (ibuffer-switch-to-saved-filter-groups "home")))

(setq ibuffer-saved-filter-groups
      '(("home"
	 ("emacs-config" (or (filename . ".emacs.d")
			     (filename . ".emacs")))
	 ("Org" (or (mode . org-mode)
		    (filename . "OrgMode")))
	 ("Web Dev" (or (mode . html-mode)
			(mode . css-mode)))
	 ("Magit" (or (mode . magit-mode)
                      (mode . magit-diff-mode)
                      (filename . "magit")))
         ("code" (filename . "code"))
	 ("Help" (or (name . "\*Help\*")
		     (name . "\*Apropos\*")
		     (name . "\*info\*"))))))


(setq ibuffer-show-empty-filter-groups nil)
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)


;;;;;;;;;;;;;;;;;;;;;;; DEFINE FUNCTIONS

(defun toggle-comment-on-line ()
  "COMMENT OR UNCOMMENT CURRENT LINE."
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))


(defalias 'yes-or-no-p 'y-or-n-p)

(defun call-task-org-capture ()
  "Initialize Org capture with task t."
  (interactive)
  (org-capture :key "t"))

;;;;;;;;;;;;;;;;;;;;;;; SETUP KEY BINDINGS
(global-set-key (kbd "C-c ,") 'er/expand-region)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-c C-f") 'projectile-find-file)
(global-set-key (kbd "C-c c") 'compile)
(global-set-key (kbd "C-;") 'toggle-comment-on-line)
(global-set-key (kbd "C-c C-n") 'highlight-symbol-next)
(global-set-key (kbd "C-c C-p") 'highlight-symbol-prev)
(global-set-key (kbd "C-c r") 'highlight-symbol-query-replace)
(global-set-key (kbd "C-c m w") 'mc/mark-all-symbols-like-this)
(global-set-key (kbd "C-c m e") 'mc/edit-lines)
(global-set-key (kbd "C-c m r") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c s") 'swiper)
(global-set-key (kbd "C-x b") 'swiper-multi)
(global-set-key (kbd "C-c t") 'call-task-org-capture)

(define-key dired-mode-map (kbd "C-)") 'dired-details-toggle)

(global-set-key (kbd "C-c h")  'windmove-left)
(global-set-key (kbd "C-c l") 'windmove-right)
(global-set-key (kbd "C-c k")    'windmove-up)
(global-set-key (kbd "C-c j")  'windmove-down)
(global-set-key (kbd "C-c C-m")  'mingus)

(global-set-key (kbd "C-c C-b")  'bookmark-set)

(global-set-key (kbd "C-c C-s")  'magit-status)


(define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)

(put 'downcase-region 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'upcase-region 'disabled nil)
(add-hook 'python-mode-hook
		  (lambda ()
		    (setq-default indent-tabs-mode t)
		    (setq-default tab-width 4)
		    (setq-default py-indent-tabs-mode t)
			(auto-complete-mode)
			(add-to-list 'write-file-functions 'delete-trailing-whitespace)))
(add-hook 'python-mode-hook 'jedi:setup)

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )


;; Ivy Mode stuff
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(setq helm-M-x-fuzzy-match                  t
      helm-bookmark-show-location           t
      helm-buffers-fuzzy-matching           t
      helm-completion-in-region-fuzzy-match t
      helm-file-cache-fuzzy-match           t
      helm-imenu-fuzzy-match                t
      helm-mode-fuzzy-match                 t
      helm-locate-fuzzy-match               t
      helm-quick-update                     t
      helm-recentf-fuzzy-match              t
      helm-semantic-fuzzy-match             t)

;; enable this if you want `swiper' to use it
(setq search-default-mode #'char-fold-to-regexp)
(global-set-key "\C-s" 'swiper-helm)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

;; set actions when running C-x b
;; replace "frame" with window to open in new window
(ivy-set-actions
 'ivy-switch-buffer
 '(("j" switch-to-buffer-other-frame "other frame")
   ("k" kill-buffer "kill")
   ("r" ivy--rename-buffer-action "rename")))
